import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home'
import Blog from '../views/Blog'
import Todo from '../views/Todo'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/blog',
      component: Blog
    },
    {
      path: '/todo',
      component: Todo
    }
  ]
})
